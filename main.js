let tabs = [...document.querySelectorAll('.section-our-services-menu-tab')];
let serviceContent = [...document.querySelectorAll('.section-our-services-menu-tab-content')];

let amazTabs = [...document.querySelectorAll('.section-amazing-work-menu-tab')];
let allPic = [...document.querySelectorAll('.all-amaz-pictures')];
let buttonMore = document.querySelector('.section-amazing-work-button-more');


function randomInteger(min, allPicLength) {
    let rand = min + Math.random() * (allPicLength + 1 - min);
    return Math.floor(rand);
}

while ($(allPic).siblings('.shown').length !== 12) {
    let min = 0;
    let integ = randomInteger(min, allPic.length);
    if ($(allPic[integ]).hasClass('.shown')) {
    } else {
        $(allPic[integ]).addClass('shown');
        $(allPic[integ]).show();
    }
}


$(tabs).click(function (event) {
    let i = tabs.indexOf(event.target);
    $(tabs).siblings().removeClass('tab-clicked-style');
    $(tabs).siblings().removeClass('triangle-toolkit');
    $(serviceContent).siblings().addClass('hidden-service-content');


    $(tabs[i]).addClass('tab-clicked-style');
    $(tabs[i]).addClass('triangle-toolkit');
    $(serviceContent[i]).removeClass('hidden-service-content');
});

let amazPictureCategory;

$(amazTabs).click(function (event) {
    let i = amazTabs.indexOf(event.target);
    $(amazTabs).siblings().removeClass('amazing-work-tab-clicked');
    $(amazTabs[i]).addClass('amazing-work-tab-clicked');

    amazPictureCategory = $(event.target).data('category');

    if (amazPictureCategory === '.all-amaz-pictures') {
        $(allPic).siblings().hide();
        $(allPic).siblings('.shown').show();
        if ($(allPic).siblings('.shown').length !== 36) {
            $(buttonMore).show();
        } else {
            $(buttonMore).hide();
        }

    } else {
        $(allPic).siblings().hide();
        $(buttonMore).hide();
        $(allPic).siblings(`${amazPictureCategory}`).show();
    }
});

let loader = document.querySelector('.loader1');

$(buttonMore).click(function () {
    $(buttonMore).hide();
    $(loader).show();
    setTimeout(function () {
        $(loader).hide();
        $(buttonMore).show();
        let shownPicLength = $(allPic).siblings('.shown').length;
        shownPicLength += 12;
        while ($(allPic).siblings('.shown').length !== shownPicLength) {
            let min = 0;
            integ = randomInteger(min, allPic.length);
            if ($(allPic[integ]).hasClass('.shown')) {
            } else {
                $(allPic[integ]).addClass('shown');
                $(allPic[integ]).show();
                if (shownPicLength > 24) {
                    $(buttonMore).hide();
                }
            }
        }
    }, 2000)

});


/*______________________________________CAROUSEL___________________________*/

let carousel = [...document.querySelectorAll('.image-on-carousel')];
let carButtonLeft = document.querySelector('.button-left');
let carButtonRight = document.querySelector('.button-right');

let carouselContent = [...document.querySelectorAll('.section-about-content-person')];

$(carousel).click(function (event) {
    let i = carousel.indexOf(event.target);
    $(carousel).siblings().removeClass('on-carousel-clicked-img');
    $(carousel[i]).addClass('on-carousel-clicked-img');

    $(carouselContent).siblings().addClass('hide-person-content');
    $(carouselContent[i]).removeClass('hide-person-content');

});

$(carButtonLeft).click(function () {
    let clickedElement = document.querySelector('.on-carousel-clicked-img');
    let elem = clickedElement.previousElementSibling;
    let parent = document.querySelector('.carousel-wrapper');
    let lastElem = parent.lastElementChild;

    $(clickedElement).removeClass('on-carousel-clicked-img');

    let id = $(elem).data('id');
    let lastElemId = $(lastElem).data('id');

    $(carouselContent).siblings().addClass('hide-person-content');

    if (elem) {
        $(elem).addClass('on-carousel-clicked-img');
        $(carouselContent[id]).removeClass('hide-person-content');
    } else {
        $(lastElem).addClass('on-carousel-clicked-img');
        $(carouselContent[lastElemId]).removeClass('hide-person-content');
    }
});

$(carButtonRight).click(function () {
    let clickedElement = document.querySelector('.on-carousel-clicked-img');
    let elem = clickedElement.nextElementSibling;
    let parent = document.querySelector('.carousel-wrapper');
    let firstElem = parent.firstElementChild;

    $(clickedElement).removeClass('on-carousel-clicked-img');
    $(carouselContent).siblings().addClass('hide-person-content');

    let id = $(elem).data('id');
    let firstElemId = $(firstElem).data('id');

    if (elem) {
        $(elem).addClass('on-carousel-clicked-img');
        $(carouselContent[id]).removeClass('hide-person-content');
    } else {
        $(firstElem).addClass('on-carousel-clicked-img');
        $(carouselContent[firstElemId]).removeClass('hide-person-content');
    }
});

$('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 370,
    gutter: 15
});

let buttonBest = document.querySelector('.section-best-images-button-more');
let loader2 = document.querySelector('.loader2');


$(buttonBest).click(function () {
    $(loader2).show();
    $(buttonBest).hide();
    setTimeout(function () {
        $(loader2).hide();

        let $addElements = $('[hidden]');
        $addElements.removeAttr('hidden');

        $('.grid').append($addElements).masonry('appended', $addElements);

    }, 2000)

});